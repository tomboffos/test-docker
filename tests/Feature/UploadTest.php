<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Maatwebsite\Excel\Facades\Excel;
use Tests\TestCase;

class UploadTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $user = User::factory()->create([
            'email_verified_at' => null,
        ]);

        $file = UploadedFile::fake()->create('cards.xlsx');


        $response = $this->actingAs($user)->withHeaders([
            'Content-type' => 'multipart/form-data'
        ])->post('/upload-file',[
            'file' => $file
        ]);

        $response->assertStatus(200);
    }
}
