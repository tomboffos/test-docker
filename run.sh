cp .env.example .env

docker-compose up -d --build
docker-compose exec app php artisan key:generate
docker-compose exec app php artisan migrate
docker-compose run --rm npm install
docker-compose run --rm npm run build
