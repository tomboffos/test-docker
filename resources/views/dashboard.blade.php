<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    @if($errors->has('file'))
                        <div class="error">{{ $errors->first('file') }}</div>
                    @endif
                    <form action="{{route('upload')}}" enctype="multipart/form-data" method="POST">
                        {{csrf_field()}}
                        <div class="form-control max-w-6xl">
                            <input type="file" name="file" class="form-input">
                        </div>
                        <button type="submit" class="">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                @foreach($data as $file)
                    @if(is_readable($file->link))
                        <div class="p-6 text-gray-900 dark:text-gray-100">
                            <a href="{{$file->link}}">{{$file->name}}</a>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>


</x-app-layout>
