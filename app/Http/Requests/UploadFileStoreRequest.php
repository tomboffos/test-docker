<?php

namespace App\Http\Requests;

use App\Domain\UploadFile\Data\UploadFileStoreDto;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\File;

class UploadFileStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return 1;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'file' => ['required', File::types(['xlsx'])]
        ];
    }

    public function toDto(): UploadFileStoreDto
    {
        return new UploadFileStoreDto(user: $this->user(), file: $this->file('file'),);
    }
}
