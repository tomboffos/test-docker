<?php

namespace App\Http\Controllers\Card;

use App\Domain\UploadFile\Contracts\UploadFileRepositoryInterface;
use App\Domain\UploadFile\Contracts\UploadFileServiceInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\UploadFileStoreRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CardController extends Controller
{
    public function __construct(
        private UploadFileServiceInterface $uploadFileService
    )
    {

    }

    public function index(Request $request)
    {
        return view('dashboard', [
            'data' => $this->uploadFileService->getFiles($request->user())
        ]);
    }

    public function upload(UploadFileStoreRequest $request): RedirectResponse
    {
        $uploadFile = $this->uploadFileService->createNewXlsxFile($request->toDto());

        if (is_readable($uploadFile->link))
            return redirect()->to($uploadFile->link);
        return back()->withErrors([
            'file' => 'File not saved'
        ]);
    }
}
