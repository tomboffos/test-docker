<?php

namespace App\Domain\UploadFile\Data;

use App\Models\User;
use Illuminate\Http\UploadedFile;

class UploadFileStoreDto
{
    public function __construct(
        public User         $user,
        public UploadedFile $file,
    )
    {

    }

    public function toArray(): array
    {
        return [
            'user_id' => $this->user->id,
            'file' => $this->file,
            'name' => $this->file->getClientOriginalName()
        ];
    }
}
