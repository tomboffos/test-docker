<?php

namespace App\Domain\UploadFile\Data;

use App\Models\User;
use Illuminate\Http\UploadedFile;
use Maatwebsite\Excel\Facades\Excel;

class CreatedFileDto
{
    public function __construct(
        public User   $user,
        public string $link,
        public string $fileName,
    )
    {

    }

    public function toArray(): array
    {
        return [
            'user_id' => $this->user->id,
            'link' => $this->link,
            'name' => $this->fileName
        ];
    }
}
