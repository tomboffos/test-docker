<?php

namespace App\Domain\UploadFile\Service;

use App\Domain\UploadFile\Contracts\UploadFileRepositoryInterface;
use App\Domain\UploadFile\Contracts\UploadFileServiceInterface;
use App\Domain\UploadFile\Data\CreatedFileDto;
use App\Domain\UploadFile\Data\UploadFileStoreDto;
use App\Exports\CardExport;
use App\Imports\CardImport;
use App\Models\UploadFile;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class UploadFileService implements UploadFileServiceInterface
{
    private string $directoryPath = 'public/imported/';

    public function __construct(
        private UploadFileRepositoryInterface $uploadFileRepository,
    )
    {

    }


    public function createNewXlsxFile(UploadFileStoreDto $dto): UploadFile
    {
        $dataArray = $this->getDataFromImportingFile($dto->file);


        $path = $this->saveDataToStorageFile($dataArray, $dto->file);

        return $this->uploadFileRepository->createRecord(new CreatedFileDto(user: $dto->user, link: $path, fileName: $dto->file->getClientOriginalName(),));
    }

    function saveDataToStorageFile(array $dataArray, UploadedFile $file): string
    {
        $filePath = $this->directoryPath . $file->getFilename() . '_' . Str::random(10) . '.' . $file->getClientOriginalExtension();

        Excel::store(new CardExport($dataArray), $filePath);

        return $filePath;
    }


    function getDataFromImportingFile(UploadedFile $file): array
    {
        return Excel::toArray(new CardImport(), $file);
    }


    public function getFiles(User $user): Collection
    {
        return $this->uploadFileRepository->getFilesByUser($user);
    }
}
