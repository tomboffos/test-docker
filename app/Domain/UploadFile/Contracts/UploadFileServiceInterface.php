<?php

namespace App\Domain\UploadFile\Contracts;

use App\Domain\UploadFile\Data\UploadFileStoreDto;
use App\Models\UploadFile;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;

interface UploadFileServiceInterface
{
    public function getFiles(User $user): Collection;

    public function createNewXlsxFile(UploadFileStoreDto $dto): UploadFile;

    function saveDataToStorageFile(array $dataArray, UploadedFile $file): string;

    function getDataFromImportingFile(UploadedFile $file): array;
}
