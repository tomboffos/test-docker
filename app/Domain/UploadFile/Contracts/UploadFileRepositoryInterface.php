<?php

namespace App\Domain\UploadFile\Contracts;

use App\Domain\UploadFile\Data\CreatedFileDto;
use App\Models\UploadFile;
use App\Models\User;
use Illuminate\Support\Collection;

interface UploadFileRepositoryInterface
{
    public function createRecord(CreatedFileDto $dto) : UploadFile;

    public function getFilesByUser(User $user) : Collection;
}
