<?php

namespace App\Domain\UploadFile\Contracts;

interface UploadFileContract
{
    const NAME = 'name';
    const LINK = 'link';
    const USER_ID = 'user_id';


    const FILLABLE = [
        self::LINK,
        self::NAME,
        self::USER_ID,
    ];
}
