<?php

namespace App\Domain\UploadFile\Repository;

use App\Domain\UploadFile\Contracts\UploadFileContract;
use App\Domain\UploadFile\Contracts\UploadFileRepositoryInterface;
use App\Domain\UploadFile\Data\CreatedFileDto;
use App\Models\UploadFile;
use App\Models\User;
use Illuminate\Support\Collection;

class UploadFileRepository implements UploadFileRepositoryInterface
{


    public function createRecord(CreatedFileDto $dto): UploadFile
    {
        return UploadFile::query()->create($dto->toArray());
    }

    public function getFilesByUser(User $user): Collection
    {
        return UploadFile::query()->where(UploadFileContract::USER_ID, $user->id)->get();
    }
}
