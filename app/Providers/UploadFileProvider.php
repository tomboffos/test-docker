<?php

namespace App\Providers;

use App\Domain\UploadFile\Contracts\UploadFileRepositoryInterface;
use App\Domain\UploadFile\Contracts\UploadFileServiceInterface;
use App\Domain\UploadFile\Repository\UploadFileRepository;
use App\Domain\UploadFile\Service\UploadFileService;
use Illuminate\Support\ServiceProvider;

class UploadFileProvider extends ServiceProvider
{

    public function register(): void
    {
        $this->app->bind(UploadFileRepositoryInterface::class, UploadFileRepository::class);
        $this->app->bind(UploadFileServiceInterface::class, UploadFileService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
