<?php

namespace App\Providers;

use App\Domain\UploadFile\Contracts\UploadFileRepositoryInterface;
use App\Domain\UploadFile\Repository\UploadFileRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    public function register() : void
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
