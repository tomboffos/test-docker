<?php

namespace App\Infrastructure\Gateways\ApiLayer\Contract;

interface ApiLayerGatewayInterface
{
    public static function getDataByCardNumber(string $number) : array;
}
