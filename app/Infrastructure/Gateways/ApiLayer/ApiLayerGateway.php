<?php

namespace App\Infrastructure\Gateways\ApiLayer;

use App\Infrastructure\Gateways\ApiLayer\Contract\ApiLayerGatewayInterface;
use Illuminate\Support\Facades\Http;

class ApiLayerGateway implements ApiLayerGatewayInterface
{
    const API_KEY = '0eqR5lpdD5GJ0mlOtnEu3Oe1vYIFcuXB';

    public static function getDataByCardNumber(string $number): array
    {
        $response = Http::get("https://api.apilayer.com/bincheck/{$number}?apikey=" . self::API_KEY);
        return [
            'scheme' => $response->json()['scheme'] ?? '',
            'type' => $response->json()['type'] ?? '',
            'bank_name' => $response->json()['bank_name'] ?? '',
        ];
    }
}
