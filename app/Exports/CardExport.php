<?php

namespace App\Exports;

use App\Infrastructure\Gateways\ApiLayer\ApiLayerGateway;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class CardExport implements FromView
{
    public function __construct(private array $data)
    {

    }

    public function view(): View
    {
        $mainData = [];
        foreach ($this->data[0] as $item) {
            $array = array_filter($item, fn($value) => !is_null($value) && $value !== '');
            $response = ApiLayerGateway::getDataByCardNumber(end($array));

            $array[] = $response['scheme'].' '.$response['type'];
            $array[] = $response['bank_name'];

            $mainData[] = $array;
        }

        return view('export.card', [
            'data' => $mainData
        ]);
    }
}
