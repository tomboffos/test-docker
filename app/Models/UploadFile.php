<?php

namespace App\Models;

use App\Domain\UploadFile\Contracts\UploadFileContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class UploadFile extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = UploadFileContract::FILLABLE;

    public function getLinkAttribute() : string
    {
        return str_replace('public', 'storage', $this->attributes[UploadFileContract::LINK]);
    }
}
